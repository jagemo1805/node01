const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    res.send('hello express');
})

app.post('/hello', (req, res) => {
    console.log(req.body);
    let body = req.body;
    body.message = "hello " + body.name;
    res.json(body);
})

app.listen(3000, () => {
    console.log('server listens on 3000');
});